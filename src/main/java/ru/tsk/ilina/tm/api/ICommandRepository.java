package ru.tsk.ilina.tm.api;

import ru.tsk.ilina.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();
}
